﻿namespace GitignoreGenerator
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonGenerateGitignore = new System.Windows.Forms.Button();
            this.listBoxPwnUnignoreLines = new System.Windows.Forms.ListBox();
            this.listBoxGitignoreLines = new System.Windows.Forms.ListBox();
            this.labelPwnUnignore = new System.Windows.Forms.Label();
            this.labelGitignore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonGenerateGitignore
            // 
            this.buttonGenerateGitignore.Location = new System.Drawing.Point(12, 12);
            this.buttonGenerateGitignore.Name = "buttonGenerateGitignore";
            this.buttonGenerateGitignore.Size = new System.Drawing.Size(171, 23);
            this.buttonGenerateGitignore.TabIndex = 0;
            this.buttonGenerateGitignore.Text = "Generate Gitignore";
            this.buttonGenerateGitignore.UseVisualStyleBackColor = true;
            this.buttonGenerateGitignore.Click += new System.EventHandler(this.buttonGenerateGitignore_Click);
            // 
            // listBoxPwnUnignoreLines
            // 
            this.listBoxPwnUnignoreLines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.listBoxPwnUnignoreLines.FormattingEnabled = true;
            this.listBoxPwnUnignoreLines.Location = new System.Drawing.Point(12, 84);
            this.listBoxPwnUnignoreLines.Name = "listBoxPwnUnignoreLines";
            this.listBoxPwnUnignoreLines.Size = new System.Drawing.Size(441, 303);
            this.listBoxPwnUnignoreLines.TabIndex = 1;
            // 
            // listBoxGitignoreLines
            // 
            this.listBoxGitignoreLines.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxGitignoreLines.FormattingEnabled = true;
            this.listBoxGitignoreLines.Location = new System.Drawing.Point(459, 84);
            this.listBoxGitignoreLines.Name = "listBoxGitignoreLines";
            this.listBoxGitignoreLines.Size = new System.Drawing.Size(505, 303);
            this.listBoxGitignoreLines.TabIndex = 2;
            this.listBoxGitignoreLines.SelectedIndexChanged += new System.EventHandler(this.listBoxGitignoreLines_SelectedIndexChanged);
            // 
            // labelPwnUnignore
            // 
            this.labelPwnUnignore.AutoSize = true;
            this.labelPwnUnignore.Location = new System.Drawing.Point(13, 65);
            this.labelPwnUnignore.Name = "labelPwnUnignore";
            this.labelPwnUnignore.Size = new System.Drawing.Size(35, 13);
            this.labelPwnUnignore.TabIndex = 3;
            this.labelPwnUnignore.Text = "label1";
            // 
            // labelGitignore
            // 
            this.labelGitignore.AutoSize = true;
            this.labelGitignore.Location = new System.Drawing.Point(456, 65);
            this.labelGitignore.Name = "labelGitignore";
            this.labelGitignore.Size = new System.Drawing.Size(35, 13);
            this.labelGitignore.TabIndex = 4;
            this.labelGitignore.Text = "label1";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(984, 450);
            this.Controls.Add(this.labelGitignore);
            this.Controls.Add(this.labelPwnUnignore);
            this.Controls.Add(this.listBoxGitignoreLines);
            this.Controls.Add(this.listBoxPwnUnignoreLines);
            this.Controls.Add(this.buttonGenerateGitignore);
            this.Name = "FormMain";
            this.Text = "Gitignore Generator";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonGenerateGitignore;
        private System.Windows.Forms.ListBox listBoxPwnUnignoreLines;
        private System.Windows.Forms.ListBox listBoxGitignoreLines;
        private System.Windows.Forms.Label labelPwnUnignore;
        private System.Windows.Forms.Label labelGitignore;
    }
}

