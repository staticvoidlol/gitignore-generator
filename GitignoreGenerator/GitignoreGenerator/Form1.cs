﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace GitignoreGenerator
{
    enum PatternType
    {
        DirLiteral,
        DirPattern,
        FileLiteral,
        FilePattern
    }

    public partial class FormMain : Form
    {
        bool IsSilentRun = false;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            string[] args = Environment.GetCommandLineArgs();

            if (args.Length > 1)
            {
                //MessageBox.Show(args.Count().ToString());
                if(args.Last() == "-silent")
                {
                    IsSilentRun = true;
                    WindowState = FormWindowState.Minimized;
                    ShowInTaskbar = false;
                    GenerateGitignore();
                }
            }
        }

        private void buttonGenerateGitignore_Click(object sender, EventArgs e)
        {
            // Read PWN Unignore
            List<string> pwnUnignoreLines = File.ReadLines("pwnunignore.txt").ToList();

            FileInfo pwnIgnoreFileInfo = new FileInfo("pwnunignore.txt");
            labelPwnUnignore.Text = pwnIgnoreFileInfo.FullName;
            listBoxPwnUnignoreLines.Items.Clear();
            foreach (string pwnUnignoreLine in pwnUnignoreLines)
            {
                listBoxPwnUnignoreLines.Items.Add(pwnUnignoreLine);
            }

            GetUnignoredFiles(pwnUnignoreLines);

            // Read PWN Unignore
            List<string> gitignoreLines = File.ReadLines(".gitignore.").ToList();

            FileInfo gitIgnoreFileInfo = new FileInfo(".gitignore.");
            labelGitignore.Text = pwnIgnoreFileInfo.FullName;
            listBoxGitignoreLines.Items.Clear();
            foreach (string gitignoreLine in gitignoreLines)
            {
                listBoxGitignoreLines.Items.Add(gitignoreLine);
            }
        }

        private void GenerateGitignore()
        {
            // Read PWN Unignore
            List<string> pwnUnignoreLines = File.ReadLines("pwnunignore.txt").ToList();
            GetUnignoredFiles(pwnUnignoreLines);

            if (IsSilentRun)
                Application.Exit();
        }

        PatternType GetPatternType(string pattern)
        {
            // Dir
            string lastChar = pattern[pattern.Length - 1].ToString();

            if(lastChar == "/")
            {
                if (pattern.Contains(@"*"))
                    return PatternType.DirPattern;
                else
                    return PatternType.DirLiteral;
            }
            // File
            else
            {
                if (pattern.Contains(@"*"))
                    return PatternType.FilePattern;
                else
                    return PatternType.FileLiteral;
            }
        }

        string GetPathWithForwardSlashes(string path)
        {
            return path.Replace(@"\", @"/");
        }

        string GetPathWithBackslashes(string path)
        {
            return path.Replace(@"/", @"\");
        }

        string GetPathWithDoubleBackslashes(string path)
        {
            return GetPathWithBackslashes(path.Replace(@"\", @"\\"));
        }

        string GetPathWithoutExclamation(string path)
        {
            return path.Replace(@"!", "");
        }

        string GetPathReplaceDoubleBackslashesWithForwardSlashes(string path)
        {
            return path.Replace(@"\\", "/");
        }

        private List<string> GetUnignoredFiles(List<string> pwnUnignoreLines)
        {
            //pwnUnignoreLines.Add(".git/");
            //pwnUnignoreLines.Add("GitignoreGenerator.exe");
            //pwnUnignoreLines.Add("GitignoreGenerator.exe.config");
            //pwnUnignoreLines.Add("GitignoreGenerator.pdb");
            //pwnUnignoreLines.Add("System.Management.Automation.dll");

            List<string> unignoredDirectoryPatterns = new List<string>();
            List<string> unignoredDirectoryLiterals = new List<string>();
            List<string> unignoredFilePatterns = new List<string>();
            List<string> unignoredFileLiterals = new List<string>();

            List<string> ignoredDirectoryPatterns = new List<string>();
            List<string> ignoredDirectoryLiterals = new List<string>();
            List<string> ignoredFilePatterns = new List<string>();
            List<string> ignoredFileLiterals = new List<string>();

            // Prep lines
            foreach (string line in pwnUnignoreLines)
            {
                // Ignore empty lines and comments
                if (!(line.Length == 0 || line.Substring(0, 1) == "#"))
                {                    
                    // Handle unignore lines
                    if(line.Substring(0, 1) == "!")
                    {
                        PatternType linePatternType = GetPatternType(line);
                        // If it ends in "/" it's a dir

                        switch(linePatternType)
                        {
                            case PatternType.DirLiteral:
                                unignoredDirectoryLiterals.Add(line);
                                break;
                            case PatternType.DirPattern:
                                unignoredDirectoryPatterns.Add(line);
                                break;
                            case PatternType.FileLiteral:
                                unignoredFileLiterals.Add(line);
                                break;
                            case PatternType.FilePattern:
                                unignoredFilePatterns.Add(line);
                                break;
                        } 
                    }
                    else
                    {
                        PatternType linePatternType = GetPatternType(line);
                        // If it ends in "/" it's a dir

                        switch (linePatternType)
                        {
                            case PatternType.DirLiteral:
                                ignoredDirectoryLiterals.Add(line);
                                break;
                            case PatternType.DirPattern:
                                ignoredDirectoryPatterns.Add(line);
                                break;
                            case PatternType.FileLiteral:
                                ignoredFileLiterals.Add(line);
                                break;
                            case PatternType.FilePattern:
                                ignoredFilePatterns.Add(line);
                                break;
                        }
                    }
                }

            }

            // Get all files in project
            using (StreamWriter w = new StreamWriter(Path.Combine(Directory.GetCurrentDirectory(), ".gitignore.")))
            {
                // Write to gitignore the fully excluded files and directories
                w.WriteLine("#########################################################");
                w.WriteLine("### Generated ignores as of : " +DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") +" ###");
                w.WriteLine("#########################################################");
                w.WriteLine();
                w.WriteLine("# From 'pwnunignore.txt' [Ignored Directory Literals]");
                foreach (string s in ignoredDirectoryLiterals)
                {
                    w.WriteLine(s);
                }

                w.WriteLine();
                w.WriteLine("# From 'pwnunignore.txt' [Ignored Directory Patterns]");
                foreach (string s in ignoredDirectoryPatterns)
                {
                    w.WriteLine(s);
                }

                w.WriteLine();
                w.WriteLine("# From 'pwnunignore.txt' [Ignored File Literals]");
                foreach (string s in ignoredFileLiterals)
                {
                    w.WriteLine(s);
                }

                w.WriteLine();
                w.WriteLine("# From 'pwnunignore.txt' [Ignored File Patterns]");
                foreach (string s in ignoredFilePatterns)
                {
                    w.WriteLine(s);
                }
                w.WriteLine();

                List<string> allFiles = Directory.EnumerateFiles(Directory.GetCurrentDirectory(), "*", SearchOption.AllDirectories).ToList();

                // Convert all lists to the files they represent
                string currDir = Directory.GetCurrentDirectory();              
                int counter = 0;
                List<string> ignoredFiles = new List<string>();
                foreach (string file in allFiles)
                {
                    //if (file.Equals("I:\\PWNSSDEV\\GitignoreGenerator\\GitignoreGenerator\\GitignoreGenerator\\bin\\Debug\\PWNGame\\Binaries\\Win64\\PWNGame-Win64-DebugGame.exe"))
                    if (file.Equals("I:\\PWNSSDEV\\GitignoreGenerator\\GitignoreGenerator\\GitignoreGenerator\\bin\\Debug\\PWNGame\\Binaries\\Win64\\PWNGame-Win64-DebugGame.exe"))
                        Console.WriteLine();

                    // Ignoreed Directory patterns
                    foreach (string s in ignoredDirectoryPatterns)
                    {
                        string patternString = s.Replace(@"*", @".*");
                        Regex pattern = new Regex(patternString);
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        if (pattern.IsMatch(fileTrimmed))
                        {
                            ignoredFiles.Add(file);
                        }
                    }

                    Console.WriteLine();

                    // Ignored Directory Literals
                    foreach (string s in ignoredDirectoryLiterals)
                    {
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        string patternWithoutExclamation = GetPathWithoutExclamation(s);

                        if (fileTrimmed.Length >= patternWithoutExclamation.Length)
                        {
                            if (fileTrimmed.Substring(0, patternWithoutExclamation.Length) == patternWithoutExclamation)
                                ignoredFiles.Add(file);
                        }
                    }

                    Console.WriteLine();

                    // Ignored File Patters
                    foreach (string s in ignoredFilePatterns)
                    {
                        string patternString = s.Replace(@"*", @".*");
                        Regex pattern = new Regex(patternString);
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        if (pattern.IsMatch(fileTrimmed))
                        {
                            ignoredFiles.Add(file);
                        }
                    }

                    Console.WriteLine();

                    // Ignored File Literals
                    foreach (string s in ignoredFileLiterals)
                    {
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        string patternWithoutExclamation = GetPathWithoutExclamation(s);

                        if (fileTrimmed.Length >= patternWithoutExclamation.Length)
                        {
                            if (fileTrimmed.Substring(0, patternWithoutExclamation.Length) == patternWithoutExclamation)
                                ignoredFiles.Add(file);
                        }
                    }

                    Console.WriteLine();
                }

                // Remove ignored files from the full file list
                allFiles = allFiles.Except(ignoredFiles).ToList();
                Console.WriteLine();

                List<string> includedFiles = new List<string>();                

                foreach (string file in allFiles)
                {
                    //if(file.Equals("I:\\PWNSSDEV\\GitignoreGenerator\\GitignoreGenerator\\GitignoreGenerator\\bin\\Debug\\PWNGame\\Saved\\Cooked\\WindowsNoEditor\\PWNGame\\Content\\Maps\\PWNGame-Basic.umap"))
                    //if (file.Equals("I:\\PWNSSDEV\\GitignoreGenerator\\GitignoreGenerator\\GitignoreGenerator\\bin\\Debug\\PWNGame\\Source\\PWNGame.Target.cs"))
                    if (file.Equals("I:\\PWNSSDEV\\GitignoreGenerator\\GitignoreGenerator\\GitignoreGenerator\\bin\\Debug\\PWNGame\\Content\\Maps\\PWNGame-BasicMap.umap"))
                        Console.WriteLine();                   


                    // CANT HANDLE MULTIPLE WILDCARDS!! ???
                    // Unignored Directory patterns
                    foreach (string s in unignoredDirectoryPatterns)
                    {
                        string patternString = GetPathWithoutExclamation(s.Replace(@"*", @".*"));
                        Regex pattern = new Regex(patternString);
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        
                        if (pattern.IsMatch(fileTrimmed))
                        {
                            includedFiles.Add(file);                            
                        }
                    }

                    Console.WriteLine();

                    // Unignored Directory Literals
                    foreach (string s in unignoredDirectoryLiterals)
                    {                        
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        string patternWithoutExclamation = GetPathWithoutExclamation(s);

                        if (fileTrimmed.Length >= patternWithoutExclamation.Length)
                        {
                            string fileTrimmedStart = fileTrimmed.Substring(0, patternWithoutExclamation.Length);
                            if (fileTrimmedStart == patternWithoutExclamation)
                                includedFiles.Add(file);
                        }
                        
                    }

                    // CANT HANDLE MULTIPLE WILDCARDS!!
                    // Unignored File patterns
                    
                    foreach (string s in unignoredFilePatterns)
                    {
                        string patternString = GetPathWithoutExclamation(s.Replace(@"*", @".*"));
                        Regex pattern = new Regex(patternString);
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        
                        if (pattern.IsMatch(fileTrimmed))
                        {
                            // It's an "all files in parent dir pattern"
                            string rawPatternLastChar = s.Substring(s.Length - 1, 1);
                            if (rawPatternLastChar == @"*")
                            {
                                string sChopped = GetPathWithoutExclamation(s.Substring(0, s.Length - 1));

                                List<string> filesInParentDir = Directory.EnumerateFiles(sChopped, "*", SearchOption.TopDirectoryOnly).ToList();

                                if (filesInParentDir.Contains(fileTrimmed))
                                    includedFiles.Add(file);
                            }
                            else
                            {
                                // Find location of wildcard
                                string patternStringWithoutExcl = GetPathWithoutExclamation(s);
                                int wildCardLoc = patternStringWithoutExcl.IndexOf(@"*", 0);
                                List<string> x = patternStringWithoutExcl.Split('/').ToList();
                                x.RemoveAt(x.Count - 1);
                                string parentDir = String.Join("/", x.ToArray());

                                List<string> filesInParentDir = Directory.EnumerateFiles(parentDir, "*", SearchOption.TopDirectoryOnly).ToList();

                                // Weird format returned: "PWNGame/Content/Maps\\PWNGame-BasicMap.umap"
                                for (int i = 0; i < filesInParentDir.Count; i++)
                                {
                                    filesInParentDir[i] = GetPathWithForwardSlashes(filesInParentDir[i]);
                                }

                                if (filesInParentDir.Contains(fileTrimmed))
                                    includedFiles.Add(file);

                                //int lastSlashLoc = patternStringWithoutExcl.LastIndexOf(@"/", 0);
                                //string parentDir = patternStringWithoutExcl.Substring(0, lastSlashLoc - 1);
                                //includedFiles.Add(file);
                            }
                        }
                    }
                    

                    Console.WriteLine();

                    // Unignored File Literals
                    foreach (string s in unignoredFileLiterals)
                    {
                        string fileTrimmed = GetPathWithForwardSlashes(file.Replace(currDir, "").Remove(0, 1));
                        string patternWithoutExclamation = GetPathWithoutExclamation(s);

                        if (fileTrimmed.Length >= patternWithoutExclamation.Length)
                        {
                            if (fileTrimmed.Substring(0, patternWithoutExclamation.Length) == patternWithoutExclamation)
                                includedFiles.Add(file);
                        }

                    }
                }

                Console.WriteLine();

                List<string> finalIgnores = allFiles.Except(ignoredFiles).ToList();
                finalIgnores = finalIgnores.Except(includedFiles).ToList();

                //File.WriteAllLines("XXX_allFiles.txt", allFiles.ToArray());
                //File.WriteAllLines("XXX_ignoredFiles.txt", ignoredFiles.ToArray());
                //File.WriteAllLines("XXX_includedFiles.txt", includedFiles.ToArray());                

                // Collapse folders                
                List<string> allDirs = Directory.EnumerateDirectories(Directory.GetCurrentDirectory(), "*", SearchOption.AllDirectories).ToList();
                List<string> additionalIgnoredDirs = new List<string>();

                // Order by least amount of nesting
                allDirs = allDirs.OrderBy(i => i.Count(f => f == '\\')).OrderBy(x => x).ToList();

                foreach(string s in allDirs)
                {
                    List<string> allFilesInDir = Directory.EnumerateFiles(s, "*", SearchOption.AllDirectories).ToList();

                    if (allFilesInDir.Count() > 0)
                    {
                        if (finalIgnores.Intersect(allFilesInDir).Count() == allFilesInDir.Count())
                        {
                            additionalIgnoredDirs.Add(s);
                            finalIgnores = finalIgnores.Except(allFilesInDir).ToList();
                        }
                    }
                }

                //File.WriteAllLines("XXX_AdditionalIgnoreDirs.txt", additionalIgnoredDirs.ToArray());
                //File.WriteAllLines("XXX_finalIgnores.txt", finalIgnores.ToArray());

                w.WriteLine();
                w.WriteLine("# Consolidated ignore directories.");
                foreach (string s in additionalIgnoredDirs)
                {
                    string igChopped = GetPathWithForwardSlashes(s.Replace(currDir, "").Remove(0, 1)) +@"/";
                    w.WriteLine(igChopped);
                }

                w.WriteLine();
                w.WriteLine("# Additional ignored files.");
                foreach(string s in finalIgnores)
                {
                    string igChopped = GetPathWithForwardSlashes(s.Replace(currDir, "").Remove(0, 1));
                    w.WriteLine(igChopped);
                }


            }

            return new List<string>();
        }

        private void listBoxGitignoreLines_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
